package ca.ryerson.lod.metrics.semanticstuff;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import org.junit.Test;

/**
 * @author Zoran Jeremic 2013-07-16
 */
public class DataModelManagerTest {

	@Test
	public void importFileToModelTest() {
		Model model= ModelFactory.createDefaultModel();
		String path="data/geo/geopolitical_data.rdf";
		 try {
			DataModelManager.getInstance().importFileToModel(model, path);
			 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
