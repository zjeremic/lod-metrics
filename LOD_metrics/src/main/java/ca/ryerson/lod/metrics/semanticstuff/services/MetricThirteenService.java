package ca.ryerson.lod.metrics.semanticstuff.services;


import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-24
 */
/**
 * 
 * Metric 13: Ratio of unique entitie 1-(The number of unique entities / total
 * numbers of entities of a dataset )
 * 
 */
public class MetricThirteenService extends AbstractMetricsService {
	//private HashMap<String, OntClass> instancesByUri = new HashMap<String, OntClass>();
	private List<String> uniqueInstances=new LinkedList<String>();
	//private List<String> checkedInstances=new LinkedList<String>();
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(MetricThirteenService.class);

	@Override
	public void cleanData() {
		super.cleanData();
		uniqueInstances.clear();
		//checkedInstances.clear();
	}

 

	@Override
	ServiceResults checkClassInstance(OntClass ontClass,
			ExtendedIterator<OntProperty> classproperties,
			OntResource ontResource, ServiceResults counter) {
		// TODO Auto-generated method stub
		if(checkedInstances.contains(ontResource.getURI())){
			return counter;
		}
		checkedInstances.add(ontResource.getURI());
		counter.increaseDenominator();
		@SuppressWarnings("unchecked")
		List<OntResource> classInstances = (List<OntResource>) ontClass
				.listInstances().toList();
		List<Statement> resProperties = ontResource.listProperties().toList();
		boolean isUniqueInstance = true;
 
		for (OntResource clInst : classInstances) {
			if (isUniqueInstance && !clInst.getURI().equals(ontResource.getURI())) {
				int clInstPropSize = clInst.listProperties().toList().size();
				if (clInstPropSize == resProperties.size()) {
					isUniqueInstance = checkIfInstanceIsUnique(ontResource,
							clInst,   classInstances);
				} else {
					isUniqueInstance = false;
					break;
				}
			}
		}
		if (isUniqueInstance) {
			if(!uniqueInstances.contains(ontResource.getURI())){
				uniqueInstances.add(ontResource.getURI());
				counter.increaseHits();
			}
			
		}
		return counter;
	}

	// Compare if the properties of the specific instances are the same as the
	// properties of all other instances
	// If so, then the class is not unique
	private boolean checkIfInstanceIsUnique(OntResource ontResource,
			OntResource clInst,  
			List<OntResource> classInstances) {
		boolean isUniqueInstance=false;
		//boolean RDFNode checkNode = checkStatement.getObject(); = false;
		StmtIterator instProperties = clInst.listProperties();

		while (instProperties.hasNext()) {

			Statement statement = instProperties.next();
			Property property = statement.getPredicate();
			//String prUri = statement.getPredicate().getURI();
			RDFNode node = statement.getObject();
			if (ontResource.hasProperty(property)) {
				List<Statement> checkPropertiesList = ontResource
						.listProperties(property).toList();
				if (checkPropertiesList.size() > 1) {
					List<Statement> propertiesList = clInst.listProperties(
							property).toList();
					if(compareNodesInList(propertiesList, checkPropertiesList)){
						return true;
					}
				} else {
					Statement checkStatement = ontResource
							.getProperty(property);
					RDFNode checkNode = checkStatement.getObject();
					if(compareNodes(node,checkNode)){
						return true;
					}
				}
			}
		}

		return isUniqueInstance;
	}
	private boolean compareNodesInList( List<Statement> propertiesList, List<Statement> checkPropertiesList){
		//boolean hasSame=false;
		int counter=0;
		for(Statement statement:propertiesList){
			RDFNode node = statement.getObject();
			for(Statement checkStatement:checkPropertiesList){
				RDFNode checkNode = checkStatement.getObject();
			
			if (node.isLiteral() && checkNode.isLiteral()) {
				String litValue = node.asLiteral().getString();
				String litValue2 = checkNode.asLiteral().getString();
				if (litValue.equals(litValue2)) {
					counter++;
					 
				}
			} else if (node.isResource() && checkNode.isResource()) {
				String uri1 = node.asResource().getURI();
				String uri2 = checkNode.asResource().getURI();
				if (uri1.equals(uri2)) {
					counter++;
				}
			}
			}
		}
		if(counter==propertiesList.size()){
			return false;
		}else{
			return true;
		}
	}
	private boolean compareNodes(RDFNode node, RDFNode checkNode){
	 //returns true if nodes are not the same, node is unique
		if (node.isLiteral() && checkNode.isLiteral()) {
			String litValue = node.asLiteral().getString();
			String litValue2 = checkNode.asLiteral().getString();
			if (!litValue.equals(litValue2)) {
				return true;
			}
		} else if (node.isResource() && checkNode.isResource()) {
			String uri1 = node.asResource().getURI();
			String uri2 = checkNode.asResource().getURI();
			if (!uri1.equals(uri2)) {
				return true;
			}
		}
		return false;
		
	}

}
