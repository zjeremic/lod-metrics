package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.List;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntResource;

/**
 * @author Zoran Jeremic 2013-07-28
 */
public class ModelStatistics {
	public ModelStats getModelStats(OntModel onto){
		ModelStats stats=new ModelStats();
		
		List<OntClass> classes=onto.listClasses().toList();
		stats.classesNo=classes.size();
		int numberOfInstances=0;
		int numberOfTriples=0;
		for(OntClass ontClass:classes){
			@SuppressWarnings("unchecked")
			List<OntResource> classInstList=(List<OntResource>) ontClass.listInstances().toList();
			int instNo=classInstList.size();
			numberOfInstances=numberOfInstances+instNo;
			for(OntResource instance:classInstList){
				int triplesSize=instance.listProperties().toList().size();
				numberOfTriples=numberOfTriples+triplesSize;
			}
			
		}
		stats.instancesNo=numberOfInstances;
		int numbOfProperties=onto.listAllOntProperties().toList().size();
		stats.propertiesNo=numbOfProperties;
		stats.triplesNo=numberOfTriples;
		 
		
		return stats;
	}

	public class ModelStats{
		public int classesNo;
		public int instancesNo;
		public int propertiesNo;
		public int triplesNo;
		
	}
}
