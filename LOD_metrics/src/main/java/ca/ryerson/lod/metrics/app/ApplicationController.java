package ca.ryerson.lod.metrics.app;

import java.net.URL;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.bridge.SLF4JBridgeHandler;
import ca.ryerson.lod.metrics.semanticstuff.services.MetricsManager;

/**
 * @author Zoran Jeremic 2013-07-19
 */
public class ApplicationController {
	private static final Logger logger = Logger.getLogger(ApplicationController.class);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	 
		String log4j="config/log4j.properties";
 URL url=ApplicationController.class.getClassLoader().getResource(log4j);
		PropertyConfigurator.configure(url);
		//	DOMConfigurator.configure(BeforeContextLoader.class.getClassLoader().getResource(settings.config.log4j));
		SLF4JBridgeHandler.install();
		logger.info("Logging has been enabled");	
		MetricsManager mm=new MetricsManager();
		//mm.runAllMetrics();
		String choice="";
		while (! choice.equals("exit")){
			System.out.println("SELECT DATASET TO RUN. (Example: DS1, DS2, DS3...DS5");
			System.out.println("TO FINISH SELECT exit");
			choice = System.console().readLine();
			if(!choice.equals("exit")){
				boolean finished=mm.runBasedOnInput(choice);
				if(finished){
					System.out.println("Task finished. Please choose another action.");
				}else{
					System.out.println("Please try again.");
				}
			}
		}  

	}

}
