package ca.ryerson.lod.metrics.semanticstuff.rdfpersistance;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

 

 

/**
 * @author Zoran Jeremic 2013-07-17
 */
public class DataSets {
	private HashMap<DataSetType, DataSet> dataSets=new HashMap<DataSetType, DataSet>();
	
	private static class DataSetsHolder {
		private static final DataSets INSTANCE = new DataSets();
	}

	public static DataSets getInstance() {
		return DataSetsHolder.INSTANCE;
	}
	
	public DataSets(){
		File dataFileRoot=new File("data"); 
		/*
		 * Data set DS1 - 
		 */
		List<String> files1=getDataSetsFiles("DS1",dataFileRoot);
		DataSet ds1=new DataSet(files1,
				"http://www.fao.org/aims/aos/fi/water_FAO_areas.owl#");
		dataSets.put(DataSetType.DS1, ds1);
		
		/*
		 * Data set DS2 - 
		 */
		List<String> files2=getDataSetsFiles("DS2",dataFileRoot);
		DataSet ds2=new DataSet(files2,
				"http://www.fao.org/aims/aos/fi/water_EEZ.owl#");
		dataSets.put(DataSetType.DS2, ds2);
		
		/*
		 * Data set DS3 - 
		 */
		List<String> files3=getDataSetsFiles("DS3",dataFileRoot);
		DataSet ds3=new DataSet(files3,
				"http://www.fao.org/aims/aos/fi/water_LME.owl#");
		dataSets.put(DataSetType.DS3, ds3);
		
		
		/*
		 * Data set DS4 - 
		 */
		List<String> files4=getDataSetsFiles("DS4",dataFileRoot);
		DataSet ds4=new DataSet(files4,
				"http://aims.fao.org/aos/geopolitical.owl#");
		dataSets.put(DataSetType.DS4, ds4);
		
		/*
		 * Data set DS5 - 
		 */
		List<String> files5=getDataSetsFiles("DS5",dataFileRoot);
		DataSet ds5=new DataSet(files5,
				"http://www.fao.org/aims/aos/fi/species_ISSCAAP.owl#");
		dataSets.put(DataSetType.DS5, ds5);
		
		/*
		 * Data set DS6 - 
		 */
		List<String> files6=getDataSetsFiles("DS6",dataFileRoot);
		DataSet ds6=new DataSet(files6,
				"http://www.fao.org/aims/aos/fi/species_taxonomic.owl#");
		dataSets.put(DataSetType.DS6, ds6);
		
		/*
		 * Data set DS7 - 
		 */
		List<String> files7=getDataSetsFiles("DS7",dataFileRoot);
		DataSet ds7=new DataSet(files7,
				"http://www.fao.org/aims/aos/fi/commodities_ISSCFC_HS.owl#");
		dataSets.put(DataSetType.DS7, ds7);
		
		/*
		 * Data set DS8 - 
		 */
		List<String> files8=getDataSetsFiles("DS8",dataFileRoot);
		DataSet ds8=new DataSet(files8,
				"http://river.styx.org/ww/2010/12/cablegraph#");
		dataSets.put(DataSetType.DS8, ds8);
		
		/*
		 * Data set DS9 - 
		 */
		List<String> files9=getDataSetsFiles("DS9",dataFileRoot);
		DataSet ds9=new DataSet(files9,
				"http://meriterm.org/heartfailure/heartfailure.rdf#");
		dataSets.put(DataSetType.DS9, ds9);
		
		
		/*
		 *Test Data set 1 - FAO geopolitical ontology
		 */
		List<String> files=new LinkedList<String>();
		files.add("data/test/v2.owl");
		DataSet tds1=new DataSet(files,
				"http://www.fao.org/aims/aos/fi/vessels_GT_GRT_ISSCFV.owl#");
		dataSets.put(DataSetType.TDS1, tds1);
		
		
		/*
		 * Test Data set 3 - http://datahub.io/dataset/eurostat-rdf
		 */
		
		List<String> filest3=new LinkedList<String>();
		 filest3.add("data/eurostat/countries.rdf");
		 filest3.add("data/eurostat/estat-legis.rdf");
		 filest3.add("data/eurostat/nace_r2.rdf");
		 filest3.add("data/eurostat/nuts2008.rdf");
		DataSet tds3=new DataSet(filest3,
				"http://ec.europa.eu/eurostat/ramon/ontologies/geographic.rdf#");
		dataSets.put(DataSetType.TDS3, tds3);
		
		/*
		 *Data set 4 - STW Thesaurus for Economics
		 */
		List<String> filest4=new LinkedList<String>();
		filest4.add("data/stw/stw.rdf");
		DataSet tdst4=new DataSet(filest4,
				"http://zbw.eu/stw/");
		dataSets.put(DataSetType.TDS4, tdst4);
		
	}
	public List<String> getDataSetsFiles(final String dataset,
			File filesDir )  {
		
		File datasetDirectory = new File(filesDir, dataset);
		 FileFilter filter=new FileFilter(){

		@Override
		public boolean accept(File pathname) {
			if (pathname.isFile()) {
				return true;
			}
			return false;
		}
		 
	 } ; 
 File[] datasetFiles=datasetDirectory.listFiles(filter);
	 
 
		List<String> files = new ArrayList<String>();
		if (datasetFiles != null) {
			for(int i=0;i<datasetFiles.length;i++){
				files.add(datasetFiles[i].getPath());
			}
		 
		}
		return files;
	}
	public DataSet getDataSet(DataSetType dsType){
		if(dataSets.containsKey(dsType)){
			DataSet ds=dataSets.get(dsType);
			return ds;
		}else return null;
	}
	public List<String> getDataSetPaths(DataSetType dsType){
		DataSet ds=this.getDataSet(dsType);
		if(ds!=null){
			for(String f:ds.getFiles()){
				System.out.println("found:"+f);
			}
			return ds.getFiles();
		}else return null;
	}
	public String getDataSetNamespace(DataSetType dsType){
		DataSet ds=this.getDataSet(dsType);
		if(ds!=null){
			return ds.getNamespace();
		}else return null;
	}
	public static enum DataSetType{
		DS1, DS2, DS3, DS4, DS5, DS6, DS7, DS8, DS9, DS10, TDS1, TDS4, TDS3
	}
}
