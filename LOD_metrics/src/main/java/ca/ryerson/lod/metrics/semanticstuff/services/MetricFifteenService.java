package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * 
 * Metric 15.  Membership of disjoint classes 
 * The number of entities are defined  as being members of disjoint classes 
 *
 */
public class MetricFifteenService extends MetricEightService{
	private static final Logger logger = Logger.getLogger(MetricFifteenService.class);
	private List<OntClass> listOfDisjointClasses=new LinkedList<OntClass>();
	@Override
	Double calculateMetric(OntModel ontModel) {
		// TODO Auto-generated method stub
		ServiceResults counter=this.getMetric(ontModel);
		Double result=Double.valueOf(counter.getHitsCounter());
		logger.info(this.getClass().getSimpleName()+" RESULT:"+result+"  hits:"+counter.getHitsCounter()+"/"+counter.getDenominator());
		cleanData();
		return result;
	}
	@Override
	public void cleanData(){
		super.cleanData();
		listOfDisjointClasses.clear();
	}
	@Override
	public ServiceResults getMetric(OntModel ontModel){
		ServiceResults counter=super.getMetric(ontModel);
		int total=0;
		for(OntClass disjointClass:listOfDisjointClasses){
			@SuppressWarnings("unchecked")
			List<OntResource> instances=(List<OntResource>) disjointClass.listInstances().toList();
			total=total+instances.size();
		}
		counter.setHitsCounter(total);
		return counter;
		
	}
	@Override
	ServiceResults checkOntClass(OntClass ontClass, ServiceResults counter){
		// TODO Auto-generated method stub
		 if(checkedClasses.contains(ontClass.getURI())){
			 return counter;
		 }
		 checkedClasses.add(ontClass.getURI());
		ExtendedIterator<OntClass> disjointClasses= ontClass.listDisjointWith();
		//List<OntResource> classInstances=  (List<OntResource>) ontClass.listInstances().toList();
		int size=disjointClasses.toList().size();
		if(size>0){
			if(!listOfDisjointClasses.contains(ontClass)){
				listOfDisjointClasses.add(ontClass);
			}
		}
		while(disjointClasses.hasNext()){
			OntClass classToCheck=disjointClasses.next();
			if(!listOfDisjointClasses.contains(classToCheck)){
				listOfDisjointClasses.add(classToCheck);
			}
		}
		 
		
	return counter;
	}

}
