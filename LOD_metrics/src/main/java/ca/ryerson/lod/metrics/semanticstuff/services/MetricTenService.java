package ca.ryerson.lod.metrics.semanticstuff.services;

import org.apache.log4j.Logger;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * Metric 10: % properties contain values with inappropriate precision 
 * 1-(The number of properties contains inappropriate precision values with 
 * respect to the precision defined for properties values in 
 * the schema  /  total numbers of properties defined in the schema of a dataset )
 *
 */
public class MetricTenService {
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(MetricTenService.class);
}
