package ca.ryerson.lod.metrics.semanticstuff.services;
/**
 * @author Zoran Jeremic 2013-07-22
 */
public class SpellCheckException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * @param error
     */
    public SpellCheckException(String error) {
        super(error);
    }

    public SpellCheckException(String message, Throwable cause) {
        super(message, cause);
    }
}
