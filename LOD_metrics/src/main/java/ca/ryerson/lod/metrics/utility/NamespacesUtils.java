package ca.ryerson.lod.metrics.utility;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Zoran Jeremic 2013-07-30
 */
public class NamespacesUtils {

	private List<String> namespaces = new LinkedList<String>();

	NamespacesUtils() {
		namespaces.add("http://www.w3.org/2000/01/rdf-schema");
		namespaces.add("http://www.w3.org/1999/02/22-rdf-syntax-ns");
		namespaces.add("http://www.w3.org/2002/07/owl");
		namespaces.add("http://www.w3.org/2001/XMLSchema");
		namespaces.add("http://purl.org/dc/elements/1.1/");
		namespaces.add("http://xmlns.com/foaf/0.1/");
		namespaces.add("http://www.w3.org/2004/02/skos/core");
		namespaces.add("http://www.w3.org/2001/vcard-rdf/3.0");

	}

	public static class NamespacesUtilsHolder {
		public static final NamespacesUtils INSTANCE = new NamespacesUtils();
	}

	/**
	 * 
	 * @return
	 */
	public static NamespacesUtils getInstance() {

		return NamespacesUtilsHolder.INSTANCE;
	}

	public boolean checkIfInRDFNamespace(String uri) {
		String namespace = getNamespaceFromFullName(uri);
		if (namespaces.contains(namespace)) {
			return true;
		} else {
			return false;
		}
	}

	public String getNamespaceFromFullName(String fullUri) {
		if (fullUri.contains("#")) {
			int sI = fullUri.indexOf("#");
			String val = fullUri.subSequence(0, sI).toString();

			return val;
		} else {
			return fullUri;
		}
	}

}
