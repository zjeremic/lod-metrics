package ca.ryerson.lod.metrics.semanticstuff.services;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.search.spell.PlainTextDictionary;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.search.spell.StringDistance;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * Metric 3. % triples contains misspelling data value 1-(The number of triples
 * contain properties with misspelling values / total numbers of triples in a
 * dataset)
 */
public class MetricThreeService extends AbstractMetricsService {
	private static Logger logger = Logger.getLogger(MetricThreeService.class
			.getName());
	private Set<String> indexedLanguages = new HashSet<String>();
	@Override
	public void cleanData(){
		super.cleanData();
	//indexedLanguages.clear();	
	}
	private long maxMemorytUsage = 128 * 1024 * 1024; // 128 megabytes
 	private Map<String, MemoryAwareSpellChecker> spellcheckersCache = new Hashtable<String, MemoryAwareSpellChecker>();
	@SuppressWarnings("serial")
	private Map<String, MemoryAwareSpellChecker> inMemorySpellcheckersCache = new LinkedHashMap<String, MemoryAwareSpellChecker>(
			2, 0.75f, true) {
		@Override
		protected boolean removeEldestEntry(
				Map.Entry<String, MemoryAwareSpellChecker> eldest) {
			return getSizeInBytes() > maxMemorytUsage;
		}

		public long getSizeInBytes() {
			long size = 0;
			for (MemoryAwareSpellChecker spellChecker : values()) {
				size += spellChecker.getIndexSize();
			}
			return size;
		}
	};

	// public SpellChecker checker;

	/*
	 * public MetricThreeService(){ dictionaires=new
	 * HashMap<String,SpellDictionary>(); checkers=new
	 * HashMap<String,SpellChecker>(); try { //SpellDictionary dictUS = new
	 * OpenOfficeSpellDictionary(new ZipFile("data/dictionaries/en_US.zip")) ;
	 * SpellDictionary dictUS = new OpenOfficeSpellDictionary(new
	 * File("data/dictionaries/en_US.dic")); //dictionaires.put("en", dictUS);
	 * SpellChecker checkerUS =new SpellChecker(dictUS);
	 * checkerUS.setCaseSensitive(false);
	 * System.out.println("added dictionary"); String[] tWords={"some",
	 * "sample", "junior", "example","and","radio"}; for(int
	 * i=0;i<tWords.length;i++){ Word w=checkerUS.checkSpell(tWords[i]); boolean
	 * isCorrect=checkerUS.isCorrect(tWords[i]);
	 * System.out.println(tWords[i]+":"+w+" isCorrect:"+isCorrect); }
	 * checkers.put("en",checkerUS);
	 * 
	 * //SpellDictionary dictFR = new OpenOfficeSpellDictionary(new
	 * ZipFile("data/dictionaries/fr_FR.zip")) ; SpellDictionary dictFR = new
	 * OpenOfficeSpellDictionary(new File("data/dictionaries/fr_FR.dic"));
	 * dictionaires.put("fr", dictFR); SpellChecker checkFR=new
	 * SpellChecker(dictFR); checkFR.setCaseSensitive(false);
	 * checkers.put("fr",checkFR);
	 * 
	 * //SpellDictionary dictRU = new OpenOfficeSpellDictionary(new
	 * ZipFile("data/dictionaries/ru_RU.zip")) ; SpellDictionary dictRU = new
	 * OpenOfficeSpellDictionary(new File("data/dictionaries/ru_RU.dic"));
	 * dictionaires.put("ru", dictRU); SpellChecker checkRU=new
	 * SpellChecker(dictRU); checkRU.setCaseSensitive(false);
	 * checkers.put("ru",checkRU);
	 * 
	 * SpellDictionary dictES = new OpenOfficeSpellDictionary(new
	 * File("data/dictionaries/es_ES.dic")); dictionaires.put("es", dictES);
	 * SpellChecker checkES=new SpellChecker(dictES);
	 * checkES.setCaseSensitive(false); checkers.put("es",checkES);
	 * 
	 * // SpellDictionary dictIT = new OpenOfficeSpellDictionary(new
	 * ZipFile("data/dictionaries/it_IT.zip")) ; SpellDictionary dictIT = new
	 * OpenOfficeSpellDictionary(new File("data/dictionaries/it_IT.dic"));
	 * dictionaires.put("it", dictIT); SpellChecker checkIT=new
	 * SpellChecker(dictIT); checkIT.setCaseSensitive(false);
	 * checkers.put("it",checkIT);
	 * 
	 * //SpellDictionary dictAR = new OpenOfficeSpellDictionary(new
	 * ZipFile("data/dictionaries/ar_AR.zip")) ; SpellDictionary dictAR = new
	 * OpenOfficeSpellDictionary(new File("data/dictionaries/ar_AR.dic"),new
	 * File("data/dictionaries/ar_AR.aff")); dictionaires.put("ar", dictAR);
	 * SpellChecker checkAR=new SpellChecker(dictAR);
	 * checkAR.setCaseSensitive(false); checkers.put("ar",checkAR);
	 * 
	 * 
	 * } catch (IOException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); }
	 * 
	 * 
	 * }
	 */
	/*
	 * public boolean checkSpelling(String value, String lang) {
	 * System.out.println(lang+":/Check spelling for:"+value); String[] words =
	 * value.split("\\s+"); boolean hasMisspellingValues=false; SpellDictionary
	 * dict=null; for (int i = 0; i < words.length; i++) {
	 * 
	 * String stringWord = words[i];
	 * 
	 * //.replaceAll(".", ""); if(stringWord.endsWith(".")){
	 * stringWord=stringWord.replace(".", ""); }
	 * 
	 * // SpellDictionary dictUS=null; // try { // dictUS = new
	 * OpenOfficeSpellDictionary(new ZipFile("data/dictionaries/en_US.zip")); //
	 * } catch (IOException e1) { // // TODO Auto-generated catch block //
	 * e1.printStackTrace(); // } //dictionaires.put("en", dictUS);
	 * //SpellChecker checker=new SpellChecker(dictUS);
	 * //checkers.put("en",checkUS); //SpellChecker checker =null;
	 * if(!lang.equals("ar")){ return false; } if(checkers.containsKey(lang)){
	 * checker=checkers.get(lang); }else{
	 * 
	 * System.out.println("PROBLEM: There is no dictionary for:"+lang); return
	 * false; } //SpellChecker checker = new SpellChecker(dict) ; //
	 * checker.setCaseSensitive(false); // checkers.get("it");
	 * System.out.println("CHECKING:"+stringWord); try{ boolean
	 * isCorrect=checker.isCorrect(stringWord); // Word
	 * word=checker.checkSpell(stringWord); // if(word!=null){ //
	 * hasMisspellingValues=true; // }
	 * System.out.println("metric three finished:"
	 * +stringWord+"isCorrect:"+isCorrect); }catch(NullPointerException npe){
	 * try {
	 * System.out.println("problem to process word:"+stringWord+" with dictionary:"
	 * +lang); Thread.sleep(3000); } catch (InterruptedException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } }
	 * 
	 * 
	 * }
	 * 
	 * return hasMisspellingValues; }
	 */
	protected List<String> findSuggestions(String word, String lang,
			int maxSuggestions) throws SpellCheckException {
		SpellChecker checker = (SpellChecker) getChecker(lang); 
		try {
			String[] suggestions = checker.suggestSimilar(word, maxSuggestions);
			return Arrays.asList(suggestions);
		} catch (IOException e) {
			logger.info("Failed to find suggestions" + e);
			 throw new SpellCheckException("Failed to find suggestions", e);
		}
	}

	private MemoryAwareSpellChecker loadSpellChecker(final String lang)
			throws SpellCheckException {
		MemoryAwareSpellChecker checker = null;
		if (!indexedLanguages.contains(lang)) {
			indexedLanguages.add(lang);
			checker = reindexSpellchecker(lang);
		} else {
			try {
				checker = new MemoryAwareSpellChecker(
						getSpellCheckerDirectory(lang));
			} catch (IOException e) {
				throw new SpellCheckException("Failed to create index", e);
			}
		}
		return checker;
	}

	private Directory getSpellCheckerDirectory(String language)
			throws IOException {
		String path = "./spellchecker/lucene/" + language;
		return FSDirectory.open(new File(path));
		//return FSDirectory.getDirectory(path);
	}

	protected List<File> getDictionaryFiles(String language)
			throws SpellCheckException {
		String pathToDictionaries = "data/dictionaries/lucene";
		File dictionariesDir = new File(pathToDictionaries);
		List<File> langDictionaries = getDictionaryFiles(language,
				dictionariesDir, language);
		if (langDictionaries.size() == 0) {
			logger.info(
					"There is no dictionaries for the language=" + language);
		}
		List<File> globalDictionaries = getDictionaryFiles("global",
				dictionariesDir, "global");

		List<File> dictionariesFiles = new ArrayList<File>();
		dictionariesFiles.addAll(langDictionaries);
		dictionariesFiles.addAll(globalDictionaries);
		return dictionariesFiles;
	}

	private List<File> getDictionaryFiles(final String lang,
			File dictionariesDir, final String prefix)
			throws SpellCheckException {
		
		File languageDictionary = new File(dictionariesDir, lang);
		 FileFilter filter=new FileFilter(){

		@Override
		public boolean accept(File pathname) {
			if (pathname.isFile()) {
				return pathname.getName().startsWith(prefix);
			}
			return false;
		}
		 
	 } ; 
 File[] languageDictionaries=languageDictionary.listFiles(filter);
	 
 
		List<File> dictionaries = new ArrayList<File>();
		if (languageDictionaries != null) {
		dictionaries.addAll(Arrays.asList(languageDictionaries));
		}
		return dictionaries;
	}

	private MemoryAwareSpellChecker reindexSpellchecker(String lang)
			throws SpellCheckException {
		MemoryAwareSpellChecker checker;
		List<File> dictionariesFiles = getDictionaryFiles(lang);
		try {
			checker = new MemoryAwareSpellChecker(
					getSpellCheckerDirectory(lang));
			checker.clearIndex();
		} catch (IOException e) {
			throw new SpellCheckException("Failed to create index", e);
		}

		for (File dictionariesFile : dictionariesFiles) {
			try {
				checker.indexDictionary(new PlainTextDictionary(
						dictionariesFile));
			} catch (IOException e) {
				logger.info("Failed to index dictionary "
						+ dictionariesFile.getAbsolutePath());

				throw new SpellCheckException("Failed to index dictionary "
						+ dictionariesFile.getAbsolutePath(), e);
			}
		}
		return checker;
	}

	protected Object getChecker(String lang) throws SpellCheckException {
		MemoryAwareSpellChecker cachedChecker = inMemorySpellcheckersCache
				.get(lang);
		if (cachedChecker == null) {
			MemoryAwareSpellChecker diskSpellChecker = spellcheckersCache
					.get(lang);
			if (diskSpellChecker == null) {
				diskSpellChecker = loadSpellChecker(lang);
				spellcheckersCache.put(lang, diskSpellChecker);
			}

			try {
				cachedChecker = new MemoryAwareSpellChecker(new RAMDirectory(
						diskSpellChecker.getSpellIndex()));
				inMemorySpellcheckersCache.put(lang, cachedChecker);
			} catch (IOException e) {
				logger.info("Failed to read index");
				throw new SpellCheckException("Failed to read index", e);
			}
		}

		return cachedChecker;
	}

	public boolean checkSpelling(String value, String lang) {
		

		  String[] words =
				  value.split("\\s+"); 
		  boolean hasMisspellingValues=false; 
		  List<String> wordsList=new LinkedList<String>();
		  for (int i = 0; i < words.length; i++) {
				   
				 String stringWord = words[i];
				  
				  //.replaceAll(".", ""); if(stringWord.endsWith(".")){
				  stringWord=stringWord.replace(".", ""); 
				  wordsList.add(stringWord);
		  }
		  try {
			 
				  List<String> misspelledWordsList =  this.findMisspelledWords(wordsList.listIterator(), lang);
				  if(misspelledWordsList.size()>0){
					 hasMisspellingValues=true;
				  } 
 
		} catch (SpellCheckException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		 
		return  hasMisspellingValues;
	}

	protected List<String> findMisspelledWords(
			Iterator<String> checkedWordsIterator, String lang)
			throws SpellCheckException {
		List<String> misspelledWordsList = new ArrayList<String>();
		SpellChecker checker = (SpellChecker) getChecker(lang);
		try {
			while (checkedWordsIterator.hasNext()) {
				String word = checkedWordsIterator.next();
				if (!word.equals("") && !checker.exist(word)) {
					misspelledWordsList.add(word);
				}
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Failed to find misspelled words", e);
			throw new SpellCheckException("Failed to find misspelled words", e);
		}
		return misspelledWordsList;
	}

	@Override
	ServiceResults checkClassInstance(OntClass ontClass,
			ExtendedIterator<OntProperty> classproperties,
			OntResource ontResource, ServiceResults counter) {
		if(checkedInstances.contains(ontResource.getURI())){
			return counter;
		}
		checkedInstances.add(ontResource.getURI());
		StmtIterator hasproperties = ontResource.listProperties();
		while (hasproperties.hasNext()) {
			boolean notmatch = false;
			Statement statement = hasproperties.next();

			// Resource s=statement.getSubject();
			//Property p = statement.getPredicate();
			RDFNode object = statement.getObject();

			// OntResource range=ranges.get(p.getURI());
			//String datatypeUri = null;

			if (object.isLiteral()) {
				Object value = object.asLiteral().getValue();

				String language = statement.getLanguage();

				if (value instanceof String && language.length() > 0) {
			 			notmatch=this.checkSpelling(value.toString(), language);
				}
	 
			}
			counter.increaseDenominator();
			if (notmatch) {
				counter.increaseHits();
			}

		}
		return counter;
	}

	private class MemoryAwareSpellChecker extends SpellChecker {
		Directory _spellIndex;

		private MemoryAwareSpellChecker(Directory spellIndex)
				throws IOException {
			super(spellIndex);
			_spellIndex = spellIndex;
		}

		private MemoryAwareSpellChecker(Directory spellIndex, StringDistance sd)
				throws IOException {
			super(spellIndex, sd);
			_spellIndex = spellIndex;
		}

		long getIndexSize() {
			if (_spellIndex instanceof RAMDirectory) {
				return ((RAMDirectory) _spellIndex).sizeInBytes();
			}
			return 0;
		}

		Directory getSpellIndex() {
			return _spellIndex;
		}
	}
	@Override
	Double calculateMetric(OntModel ontModel) {
		Double result=super.calculateMetric(ontModel);
		return (1.00-result);
	}
}
