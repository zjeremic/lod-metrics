package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * Metric 8. % entities defined as being Members of deprecated classes/properties
 * 1-(The number of entities being members of disjoined 
 * classes/properties  /  total numbers of entities in a dataset )
 *
 */
public class MetricEightService extends AbstractMetricsService{
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(MetricEightService.class);
	@SuppressWarnings("unchecked")
	@Override
	ServiceResults checkClassInstance(OntClass ontClass,
			ExtendedIterator<OntProperty> classproperties,
			OntResource ontResource, ServiceResults counter) {
		// TODO Auto-generated method stub
		if(checkedInstances.contains(ontResource.getURI())){
			return counter;
		}
		checkedInstances.add(ontResource.getURI());
		List<OntClass> disjointClasses= ontClass.listDisjointWith().toList();
		List<OntResource> classInstances=  (List<OntResource>) ontClass.listInstances().toList();
		counter.increaseDenominator(classInstances.size());
	 	for(OntClass dClass:disjointClasses){
			ExtendedIterator<OntResource> disjInstances=(ExtendedIterator<OntResource>) dClass.listInstances();
			while(disjInstances.hasNext()){
				OntResource disResource=disjInstances.next();
					if(classInstances.contains(disResource)){
					counter.increaseHits();
				}
			}
 
		}
	return counter;
	}
	@Override
	Double calculateMetric(OntModel ontModel) {
		Double result=super.calculateMetric(ontModel);
		return (1.00-result);
	}

}
