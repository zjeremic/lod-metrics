package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * Metric 12: Ratio of unique properties
 * 1-(The number of unique properties  /  total numbers 
 * of properties defined in the schema of a dataset )
 *
 */
public class MetricTwelveService extends AbstractMetricsService{
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(MetricTwelveService.class);

	@Override
	ServiceResults checkClassInstance(OntClass ontClass,
			ExtendedIterator<OntProperty> classproperties,
			OntResource ontResource, ServiceResults counter) {
		if(checkedInstances.contains(ontResource.getURI())){
			return counter;
		}
		checkedInstances.add(ontResource.getURI());
		// TODO Auto-generated method stub
		StmtIterator hasproperties=ontResource.listProperties();
		List<String> hasPropertiesList=new LinkedList<String>();
		
		while (hasproperties.hasNext()){
			counter.increaseDenominator();
			Statement statement=hasproperties.next();
			//Property property=statement.getPredicate();
			RDFNode object=statement.getObject();
			String objectValue=null;
			if(object.isLiteral()){
				 objectValue=object.asLiteral().getValue().toString();
			 
			}else if(object.isURIResource()){
				 objectValue=object.asResource().getURI();
				
			}
			if(objectValue!=null){
				if(!hasPropertiesList.contains(objectValue)){
					
					hasPropertiesList.add(objectValue);
				}else{
					counter.increaseHits();
				}
			}
			
		}
		return counter;
	}
}
