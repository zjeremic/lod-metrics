package ca.ryerson.lod.metrics.semanticstuff.services;

import ca.ryerson.lod.metrics.semanticstuff.rdfpersistance.DataSets.DataSetType;
import ca.ryerson.lod.metrics.semanticstuff.services.ModelStatistics.ModelStats;

/**
 * @author Zoran Jeremic 2013-07-25
 */
public class DataSetResults {
	private Double m1;
	private Double m2;
	private Double m3;
	private Double m4;
	private Double m5;
	private Double m6;
	private Double m7;
	private Double m8;
	private Double m9;
	private Double m10;
	private Double m11;
	private Double m12;
	private Double m13;
	private Double m14;
	private Double m15;
	private Double m16;
	private Double m17;
	private Double m18;
	private Double m19;
	private Double m20;
	private Double m21;
	private DataSetType dataset;
	private ModelStats modelStats;
	public Double getM1() {
		return m1;
	}
	public void setM1(Double m1) {
		this.m1 = m1;
	}
	public Double getM2() {
		return m2;
	}
	public void setM2(Double m2) {
		this.m2 = m2;
	}
	public Double getM3() {
		return m3;
	}
	public void setM3(Double m3) {
		this.m3 = m3;
	}
	public Double getM4() {
		return m4;
	}
	public void setM4(Double m4) {
		this.m4 = m4;
	}
	public Double getM5() {
		return m5;
	}
	public void setM5(Double m5) {
		this.m5 = m5;
	}
	public Double getM6() {
		return m6;
	}
	public void setM6(Double m6) {
		this.m6 = m6;
	}
	public Double getM7() {
		return m7;
	}
	public void setM7(Double m7) {
		this.m7 = m7;
	}
	public Double getM8() {
		return m8;
	}
	public void setM8(Double m8) {
		this.m8 = m8;
	}
	public Double getM9() {
		return m9;
	}
	public void setM9(Double m9) {
		this.m9 = m9;
	}
	public Double getM10() {
		return m10;
	}
	public void setM10(Double m10) {
		this.m10 = m10;
	}
	public Double getM11() {
		return m11;
	}
	public void setM11(Double m11) {
		this.m11 = m11;
	}
	public Double getM12() {
		return m12;
	}
	public void setM12(Double m12) {
		this.m12 = m12;
	}
	public Double getM13() {
		return m13;
	}
	public void setM13(Double m13) {
		this.m13 = m13;
	}
	public Double getM14() {
		return m14;
	}
	public void setM14(Double m14) {
		this.m14 = m14;
	}
	public Double getM15() {
		return m15;
	}
	public void setM15(Double m15) {
		this.m15 = m15;
	}
	public Double getM16() {
		return m16;
	}
	public void setM16(Double m16) {
		this.m16 = m16;
	}
	public Double getM17() {
		return m17;
	}
	public void setM17(Double m17) {
		this.m17 = m17;
	}
	public Double getM18() {
		return m18;
	}
	public void setM18(Double m18) {
		this.m18 = m18;
	}
	public Double getM19() {
		return m19;
	}
	public void setM19(Double m19) {
		this.m19 = m19;
	}
	public Double getM20() {
		return m20;
	}
	public void setM20(Double m20) {
		this.m20 = m20;
	}
	public Double getM21() {
		return m21;
	}
	public void setM21(Double m21) {
		this.m21 = m21;
	}
	public DataSetType getDataset() {
		return dataset;
	}
	public void setDataset(DataSetType dataset) {
		this.dataset = dataset;
	}
	public ModelStats getModelStats() {
		return modelStats;
	}
	public void setModelStats(ModelStats modelStats) {
		this.modelStats = modelStats;
	}
 
	
 

}
