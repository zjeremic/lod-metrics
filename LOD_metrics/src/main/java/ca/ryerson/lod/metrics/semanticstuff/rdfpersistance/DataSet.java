package ca.ryerson.lod.metrics.semanticstuff.rdfpersistance;

import java.util.List;

/**
 * @author Zoran Jeremic 2013-07-17
 */
public class DataSet { 
	private String namespace;
	private List<String> files;
	
	public List<String> getFiles() {
		return files;
	}
	public void setFile(List<String> files) {
		this.files = files;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public DataSet(List<String> files2,   String namespace){
		this.files=files2;
		this.namespace=namespace;
	}

}
