package ca.ryerson.lod.metrics.semanticstuff.services;
/**
 * @author Zoran Jeremic 2013-07-17
 */
public class ServiceResults {
	private Integer hitsCounter=0;
	private Integer denominator=0;
	public Integer getHitsCounter() {
		return hitsCounter;
	}
	public void setHitsCounter(Integer hitsCounter) {
		this.hitsCounter = hitsCounter;
	}
	public Integer getDenominator() {
		return denominator;
	}
	public void setDenominator(Integer denominatorValue) {
		this.denominator = denominatorValue;
	}
	void increaseHits(){
		this.hitsCounter++;
	}
	void increaseHits(Integer value){
		this.hitsCounter=this.hitsCounter+value;
	}
	 void increaseDenominator(){
		this.denominator++;
	}
	 void increaseDenominator(Integer value){
			this.denominator=this.denominator+value;
		}
	 

}
