package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.rdf.model.Resource;
import ca.ryerson.lod.metrics.utility.NamespacesUtils;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * Metric 7. % resources using undefined classes/properties 1-(The number of
 * entities using undefined classes/properties / total numbers of entities in a
 * dataset )
 * 
 */
public class MetricSevenService extends AbstractMetricsService {
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(MetricSevenService.class);

	@Override
	ServiceResults checkClassInstance(OntClass ontClass,
			ExtendedIterator<OntProperty> classproperties,
			OntResource ontResource, ServiceResults counter) {

//		if (checkedInstances.contains(ontResource.getURI())) {
//			return counter;
//		}
		checkedInstances.add(ontResource.getURI());
		List<String> classPropertyUris = new LinkedList<String>();
		List<Resource> rdfTypes= ontResource.listRDFTypes(true).toList();
		for(Resource rdfType:rdfTypes){
			OntResource rdfTypeOnt=rdfType.as(OntResource.class);
			 
					//JenaUtil.asOntResource(rdfType);
			 

			StmtIterator hasproperties =rdfTypeOnt.listProperties();
			while (hasproperties.hasNext()) {
				Statement propStatement = hasproperties.next();
				String predicateUri = propStatement.getPredicate().getURI();
				if(!classPropertyUris.contains(predicateUri)){

					classPropertyUris.add(predicateUri);
				}
			}
		}
		// TODO Auto-generated method stub
		boolean match = false;
		StmtIterator hasproperties = ontResource.listProperties();
		List<OntProperty> classProperties = ontClass.listDeclaredProperties()
				.toList();
	
		for (OntProperty classProperty : classProperties) {
			// classProperty.getURI();
			String predicateUri = classProperty.getURI();
			classPropertyUris.add(predicateUri);
			System.out.println("declared in ontClass:" + predicateUri);
		}

		  while(classproperties.hasNext()){
		 
		  OntProperty classproperty=classproperties.next();
		  System.out.println("ontClass:"+classproperty.getURI());
		  classPropertyUris.add(classproperty.getURI());
		  }
		while (hasproperties.hasNext()) {
			Statement propStatement = hasproperties.next();
			String predicateUri = propStatement.getPredicate().getURI();
		 
			if (!NamespacesUtils.getInstance().checkIfInRDFNamespace(
					predicateUri)) {
				if (!classPropertyUris.contains(predicateUri)) {
					System.out.println("not contains:"+ predicateUri);
					match = true;
				}// else {
				//	System.out.println("Contains:"+ predicateUri);
				//}
			}
		}
		if (match == true) {
			counter.increaseHits();
		}
		counter.increaseDenominator();
		return counter;
	}

	@Override
	Double calculateMetric(OntModel ontModel) {
		Double result = super.calculateMetric(ontModel);
		return (1.00 - result);
	}

}
