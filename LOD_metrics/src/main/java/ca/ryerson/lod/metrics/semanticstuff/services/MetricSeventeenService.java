package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.HashMap;
import java.util.Set;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * 
 * Metric 17. Redefined existing properties 
 * The number of redefined existing properties in the schema 
 *
 */
public class MetricSeventeenService extends AbstractMetricsService{
	private static final Logger logger = Logger.getLogger(MetricSeventeenService.class);

	@Override
	Double calculateMetric(OntModel ontModel) {
		// TODO Auto-generated method stub
		ServiceResults counter=this.getMetric(ontModel);
		Double result=Double.valueOf(counter.getHitsCounter());
		logger.info(this.getClass().getSimpleName()+" RESULT:"+result+"  hits:"+counter.getHitsCounter()+"/"+counter.getDenominator());
		cleanData();
		return result;
	}
	
	@Override
	ServiceResults checkClassInstance(OntClass ontClass,
			ExtendedIterator<OntProperty> classproperties,
			OntResource ontResource, ServiceResults counter) {
		if(checkedInstances.contains(ontResource.getURI())){
			return counter;
		}
		checkedInstances.add(ontResource.getURI());
		// TODO Auto-generated method stub
		StmtIterator hasproperties=ontResource.listProperties();
		HashMap<String,Integer> hasPropertiesList=new HashMap<String,Integer>();
		
		while (hasproperties.hasNext()){
			counter.increaseDenominator();
			Statement statement=hasproperties.next();
			//Property property=statement.getPredicate();
			RDFNode object=statement.getObject();
			String objectValue=null;
			if(object.isLiteral()){
				 objectValue=object.asLiteral().getValue().toString();
			 
			}else if(object.isURIResource()){
				 objectValue=object.asResource().getURI();
				
			}
			if(objectValue!=null){
				if(!hasPropertiesList.containsKey(objectValue)){
					
					hasPropertiesList.put(objectValue, 1);
				}else{
					Integer value=hasPropertiesList.get(objectValue);
					hasPropertiesList.remove(objectValue);
					value=value+1;
					hasPropertiesList.put(objectValue, value);

				}
			}
			
		}
		Set<String> keys=hasPropertiesList.keySet();
		for(String key:keys){
			Integer value=hasPropertiesList.get(key);
		
			
			if(value>1){
				counter.increaseHits(value);
			}
		}
		return counter;
	}
}
