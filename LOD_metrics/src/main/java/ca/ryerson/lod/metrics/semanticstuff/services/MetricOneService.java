package ca.ryerson.lod.metrics.semanticstuff.services;


import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-17
 */
/**
 * Metric 1. % triples contain missing data values
 * 1-(The number of triples contain properties with missing values with respect to 
 * the properties defined in the schema / total numbers of triples in a dataset)
 *
 */
public class MetricOneService extends AbstractMetricsService{
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(MetricOneService.class);
	@Override
	ServiceResults checkClassInstance(OntClass ontClass,ExtendedIterator<OntProperty> classproperties,OntResource ontResource,
			ServiceResults counter){
		if(checkedInstances.contains(ontResource.getURI())){
			return counter;
		}
		checkedInstances.add(ontResource.getURI());
			StmtIterator hasproperties=ontResource.listProperties();
		List<String> hasPropertiesList=new LinkedList<String>();
		while (hasproperties.hasNext()){
			Statement statement=hasproperties.next();
			hasPropertiesList.add(statement.getPredicate().getURI());
			
		}
		while(classproperties.hasNext()){
			OntProperty subproperty=classproperties.next();
			counter.increaseDenominator();
			if(!hasPropertiesList.contains(subproperty.getURI())){
				counter.increaseHits();
				
			}
		}
 return counter;
	}
	@Override
	Double calculateMetric(OntModel ontModel) {
		Double result=super.calculateMetric(ontModel);
		return (1.00-result);
	}
 

	
	

}
