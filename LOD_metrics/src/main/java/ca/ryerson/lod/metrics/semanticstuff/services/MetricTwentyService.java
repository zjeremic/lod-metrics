package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * 
 * Metric 20. . Ratio of Distinct properties
 * 1-(The number of distinct properties presented  /  total numbers
 *  of properties defined in the schema of a dataset )
 *
 */
public class MetricTwentyService  extends AbstractMetricsService{
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(MetricTwentyService.class);
List<String> propertiesPresented=new LinkedList<String>();
List<String> propertiesDefined=new LinkedList<String>();
@Override
public void cleanData(){
	super.cleanData();
	propertiesPresented.clear();
	propertiesDefined.clear();
}
	@Override
	ServiceResults checkClassInstance(OntClass ontClass,
			ExtendedIterator<OntProperty> classproperties,
			OntResource ontResource, ServiceResults counter) {
		
		ExtendedIterator<OntProperty> ontClassProperties=ontClass.listDeclaredProperties();
		while(ontClassProperties.hasNext()){
			OntProperty subpropertySt=ontClassProperties.next();
			//Property classProperty=subpropertySt.getPredicate();
 			if(!propertiesDefined.contains(subpropertySt.getURI())){
 				propertiesDefined.add(subpropertySt.getURI());
 				counter.increaseDenominator();
 			}
		}
		if(checkedInstances.contains(ontResource.getURI())){
			return counter;
		}
		checkedInstances.add(ontResource.getURI());
		StmtIterator hasproperties=ontResource.listProperties();
		while (hasproperties.hasNext()){
			Statement statement=hasproperties.next();
			Property p=statement.getPredicate();
			if(!propertiesPresented.contains(p.getURI())){
				propertiesPresented.add(p.getURI());
				counter.increaseHits();
			}
		}
		
//		while(classproperties.hasNext()){
// 			OntProperty subproperty=classproperties.next();
// 			
// 			if(!propertiesDefined.contains(subproperty.getURI())){
// 				System.out.println("DEFINED:"+subproperty.getURI());
// 				propertiesDefined.add(subproperty.getURI());
// 				counter.increaseDenominator();
// 			}
//		}
		return counter;
	}

}
