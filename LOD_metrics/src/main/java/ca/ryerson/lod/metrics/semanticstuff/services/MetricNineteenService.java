package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * 
 * Metric 19  Inconsistent values of properties
 * The number of properties with inconsistent values
 *
 */
public class MetricNineteenService extends AbstractMetricsService{
	private static final Logger logger = Logger.getLogger(MetricNineteenService.class);

	@Override
	ServiceResults checkClassInstance(OntClass ontClass,
			ExtendedIterator<OntProperty> classproperties,
			OntResource ontResource, ServiceResults counter) {
		// TODO Auto-generated method stub
		if(checkedInstances.contains(ontResource.getURI())){
			return counter;
		}
		checkedInstances.add(ontResource.getURI());
		StmtIterator propertiesIt=ontResource.listProperties();
		HashMap<String,String> propValue=new HashMap<String,String>();
		
		while(propertiesIt.hasNext()){
			Statement statement=propertiesIt.next();
			Property property=statement.getPredicate();
			RDFNode object=statement.getObject();
			Object value=null;
			String propertyUri=property.getURI();
			if(object.isLiteral()){
				 value=object.asLiteral().getValue();
	
			}else{
				 value=object.asResource().getLocalName();
			}
			if(value!=null){
				if(propValue.containsKey(propertyUri)){
					counter.increaseHits();
				}else{
					propValue.put(propertyUri, value.toString());
				}
			}
			
		}
		return counter;
	}
	@Override
	Double calculateMetric(OntModel ontModel) {
		// TODO Auto-generated method stub
		ServiceResults counter=this.getMetric(ontModel);
		Double result=Double.valueOf(counter.getHitsCounter());
		logger.info(this.getClass().getSimpleName()+" RESULT:"+result+"  hits:"+counter.getHitsCounter()+"/"+counter.getDenominator());
		cleanData();
		return result;
	}
}
